<?php
    //$db->logincheck('uID');

    if (isset($_POST['new']))
    {
        $catID = $_POST['categories'];
        $name = $db->escapeString($_POST['name']);
        $length = $db->escapeString($_POST['length']);
        $actors = $db->escapeString($_POST['actors']);
        $short = $db->escapeString($_POST['short']);
        $link = $db->escapeString($_POST['link']);

        if (empty($catID) || empty($name) || empty($length))
        {
            $db->showMessage("Nem adtál meg minden kötelező adatot!","danger");
        }
        else
        {
            $db->DBquery("SELECT * FROM films WHERE catID=$catID AND name='$name'");
            if ($db->numRows() != 0)
            {
                $db->showMessage("Van már ilyen nevű film!","danger");
            }
            else
            {
                $db->DBquery("INSERT INTO films VALUES(null, '$name',$catID,'$length','$actors','$short','$link' )");
                header("location: index.php?pg=".base64_encode('films_list'));
            }
        }
    }   

    if (isset($_POST['back']))
    {
        header("location: index.php?pg=".base64_encode('films_list'));
    }

    $db->toForm('name|Új film felvétele¤
    action|films_new¤
    
    label|x|Kategória: <em>*</em>¤
    select|categories|ID|name¤
    
    label|x|Cím: <em>*</em>¤
    text|name|A film címe¤
    
    label|x|Hossz: <em>*</em>¤
    text|length|A film hossza¤

    label|x|Szereplők: <em>*</em>¤
    text|actors|Szereplők¤

    label|x|Leírás:¤
    textarea|short||editor¤

    label|x|Előzetes:¤
    text|link|Link¤

    
    label|x|A <em>*</em> -gal jelölt adatok megadása kötelező!¤

    submit|new|Felvesz¤
    submit|back|Vissza¤
    ');
?>
