<?php
    session_start();
    require("../../adatok.php");
    require("../../databaseClass.php");
    $db = new db($dbhost, $dbname, $dbuser, $dbpass);
    
    $id = $_POST['param'];
    $db->DBquery("SELECT 
    films.ID AS '.aktID',
    films.name AS 'Cím',
    categories.name AS 'Kategória',
    films.actors AS 'Szereplők',
    films.length AS 'Hossz',
    (SELECT CONCAT(dir, '/', filename) FROM attachments WHERE filmID=`.aktID` AND def=1) AS 'pic'
    FROM films
    INNER JOIN categories ON categories.ID = films.catID
    WHERE categories.ID = $id");
    
    $db->toGrid('Cím', 'Szereplők', 'Kategória', 'Hossz', 'pic', 'films_show');
?>