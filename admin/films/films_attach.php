<?php
//$db->logincheck('uID');

if(isset($_POST['upload']))
{
    $total = count($_FILES['uploadFile']['name']);
    for($i=0;$i<$total;$i++)
    {
        $filename = $_FILES['uploadFile']['name'][$i];
        $tmpname = $_FILES['uploadFile']['tmp_name'][$i];
        $filesize = $_FILES['uploadFile']['size'][$i];
        $filetype = $_FILES['uploadFile']['type'][$i];
        $dir = $uploads_dir.'/'.$id;

        if($db->fileUpload($tmpname, $dir, $filename))
        {
            $db->DBquery("SELECT ID FROM attachments WHERE filmID=".$id);
            if($db->numRows() == 0)
            {
                $def = 1;
            }
            else
            {
                $def = 0;
            }
            $img = '<a href="'.$dir.'/'.$filename.'" data-lightbox="roadtrip">
                <img src="'.$dir.'/'.$filename.'" class="img-thumbnail" style="width: 250px" alt="'.$filename.'">
            </a>';
            $db->DBquery("INSERT INTO attachments VALUES(null, $id, '$dir', '$filename', '$img' ,$filesize, '$filetype', CURRENT_TIMESTAMP, $def)");
            $db->showMessage("A feltöltés sikeres!", "success");
        }
        else
        {
            $db->showMessage("Hiba a feltöltés közben!", "danger");
        }
    } 
}

if(isset($_POST['back']))
{
    header("location: index.php?pg=".base64_encode('films_list'));
}

$db->toForm('name|Új plakát feltöltése¤
action|Films_attach&id='.$id.'¤
label|x|Add meg a feltölteni kívánt fájlt:¤
file|uploadFile[]|image/*|multiple¤
submit|upload|Feltöltés¤
submit|back|Vissza');

$db->DBquery("SELECT 
ID AS '.ID',
filmID AS '.filmID',
dir AS '.dir',
filename AS 'Fájlnév',
img AS 'Plakát(ok)',
size AS 'Méret',
type AS 'Típus',
date AS 'Dátum',
def AS '.def'
FROM attachments AS `Plakát(ok)` WHERE filmID=".$id);


$_SESSION['filmID'] = $id;
   
$db->toTable('r|i|d');
?>