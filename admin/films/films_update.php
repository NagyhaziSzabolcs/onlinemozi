<?php
    //$db->logincheck('uID');

    if (isset($_POST['new']))
    {
        $catID = $_POST['categories'];
        $name = $db->escapeString($_POST['name']);
        $length = $db->escapeString($_POST['length']);
        $actors = $db->escapeString($_POST['actors']);
        $short = $db->escapeString($_POST['short']);
        $link = $db->escapeString($_POST['link']);

        if (empty($catID) || empty($name) || empty($length))
        {
            $db->showMessage("Nem adtál meg minden kötelező adatot!","danger");
        }
        else
        {
            
            $db->DBquery("UPDATE films SET name='$name',catID=$catID,length='$length',actors='$actors',short='$short',link='$link' WHERE ID=".$id);
            header("location: index.php?pg=".base64_encode('films_list'));
            
        }
    }   

    if (isset($_POST['back']))
    {
        header("location: index.php?pg=".base64_encode('films_list'));
    }
    $db->DBquery("SELECT * FROM films WHERE ID=".$id);
    $res = $db->fetchOne();

    $db->toForm('name|Filmek módosítás¤
    action|films_update&id='.$id.'¤
    
    label|x|Kategória: <em>*</em>¤
    select|categories|ID|name|'.$res['catID'].'¤
    
    label|x|Cím: <em>*</em>¤
    text|name|A film címe|value="'.$res['name'].'"¤
    
    label|x|Hossz: <em>*</em>¤
    text|length|A film hossza|value="'.$res['length'].'"¤

    label|x|Szereplők: <em>*</em>¤
    text|actors|Szereplők|value="'.$res['actors'].'"¤

    label|x|Leírás: <em>*</em>¤
    textarea|short|'.$res['short'].'|editor¤

    label|x|Előzetes:¤
    text|link|Link¤

    
    label|x|A <em>*</em> -gal jelölt adatok megadása kötelező!¤

    submit|new|Módosít¤
    submit|back|Vissza
    ');
?>
