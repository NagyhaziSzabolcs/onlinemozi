-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Gép: localhost
-- Létrehozás ideje: 2021. Már 18. 12:59
-- Kiszolgáló verziója: 10.4.14-MariaDB
-- PHP verzió: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `214a_onlinemozi`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `attachments`
--

CREATE TABLE `attachments` (
  `ID` int(11) NOT NULL,
  `filmID` int(11) NOT NULL,
  `dir` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `filename` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `img` text COLLATE utf8_hungarian_ci NOT NULL,
  `size` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `date` datetime NOT NULL,
  `def` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `attachments`
--

INSERT INTO `attachments` (`ID`, `filmID`, `dir`, `filename`, `img`, `size`, `type`, `date`, `def`) VALUES
(2, 1, 'files/1', 'kong.jpg', '<a href=\"files/1/kong.jpg\" data-lightbox=\"roadtrip\">\r\n                <img src=\"files/1/kong.jpg\" class=\"img-thumbnail\" style=\"width: 250px\" alt=\"kong.jpg\">\r\n            </a>', 108380, 'image/jpeg', '2021-03-16 09:10:48', 1),
(3, 4, 'files/4', 'monhunt.jpg', '<a href=\"files/4/monhunt.jpg\" data-lightbox=\"roadtrip\">\r\n                <img src=\"files/4/monhunt.jpg\" class=\"img-thumbnail\" style=\"width: 250px\" alt=\"monhunt.jpg\">\r\n            </a>', 32761, 'image/jpeg', '2021-03-16 09:12:26', 1),
(4, 8, 'files/8', 'venom.jpg', '<a href=\"files/8/venom.jpg\" data-lightbox=\"roadtrip\">\r\n                <img src=\"files/8/venom.jpg\" class=\"img-thumbnail\" style=\"width: 250px\" alt=\"venom.jpg\">\r\n            </a>', 205737, 'image/jpeg', '2021-03-17 11:03:32', 1),
(5, 5, 'files/5', 'blckhwk.jpg', '<a href=\"files/5/blckhwk.jpg\" data-lightbox=\"roadtrip\">\r\n                <img src=\"files/5/blckhwk.jpg\" class=\"img-thumbnail\" style=\"width: 250px\" alt=\"blckhwk.jpg\">\r\n            </a>', 628779, 'image/jpeg', '2021-03-17 11:04:07', 1),
(6, 7, 'files/7', 'thebestofme.jpg', '<a href=\"files/7/thebestofme.jpg\" data-lightbox=\"roadtrip\">\r\n                <img src=\"files/7/thebestofme.jpg\" class=\"img-thumbnail\" style=\"width: 250px\" alt=\"thebestofme.jpg\">\r\n            </a>', 476907, 'image/jpeg', '2021-03-17 11:04:29', 1),
(7, 9, 'files/9', 'annabelle.jpg', '<a href=\"files/9/annabelle.jpg\" data-lightbox=\"roadtrip\">\r\n                <img src=\"files/9/annabelle.jpg\" class=\"img-thumbnail\" style=\"width: 250px\" alt=\"annabelle.jpg\">\r\n            </a>', 112252, 'image/jpeg', '2021-03-17 11:04:44', 1),
(8, 10, 'files/10', 'saw.jpg', '<a href=\"files/10/saw.jpg\" data-lightbox=\"roadtrip\">\r\n                <img src=\"files/10/saw.jpg\" class=\"img-thumbnail\" style=\"width: 250px\" alt=\"saw.jpg\">\r\n            </a>', 196887, 'image/jpeg', '2021-03-17 11:05:00', 1),
(9, 11, 'files/11', 'yesday.jpg', '<a href=\"files/11/yesday.jpg\" data-lightbox=\"roadtrip\">\r\n                <img src=\"files/11/yesday.jpg\" class=\"img-thumbnail\" style=\"width: 250px\" alt=\"yesday.jpg\">\r\n            </a>', 229489, 'image/jpeg', '2021-03-17 11:05:18', 1),
(10, 6, 'files/6', 'avengers.jpeg', '<a href=\"files/6/avengers.jpeg\" data-lightbox=\"roadtrip\">\r\n                <img src=\"files/6/avengers.jpeg\" class=\"img-thumbnail\" style=\"width: 250px\" alt=\"avengers.jpeg\">\r\n            </a>', 640074, 'image/jpeg', '2021-03-17 11:05:40', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `categories`
--

CREATE TABLE `categories` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `categories`
--

INSERT INTO `categories` (`ID`, `name`) VALUES
(0, 'nincs megadva'),
(1, 'Akció'),
(2, 'Vígjáték'),
(3, 'Sc-fi'),
(4, 'Horror'),
(5, 'Thriller'),
(6, 'Romantikus'),
(7, 'Dráma'),
(8, 'Fantasy'),
(9, 'Háborús'),
(12, 'Összes');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `films`
--

CREATE TABLE `films` (
  `ID` int(11) NOT NULL,
  `name` varchar(70) COLLATE utf8_hungarian_ci NOT NULL,
  `catID` int(11) NOT NULL,
  `length` varchar(10) COLLATE utf8_hungarian_ci NOT NULL,
  `actors` text COLLATE utf8_hungarian_ci NOT NULL,
  `short` text COLLATE utf8_hungarian_ci NOT NULL,
  `link` text COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `films`
--

INSERT INTO `films` (`ID`, `name`, `catID`, `length`, `actors`, `short`, `link`) VALUES
(1, 'Godzilla Kong ellen', 1, '113', 'Millie Bobby Brown, Kyle Chandler, Eiza González, Rebecca Hall, Brian Tyree Henry, Jessica Henwick, Alexander Skarsgård ', 'Mind tudtuk, hogy egyszer megtörténik. Világunk szörnyei nem kerülhetik el egymást. A legnagyobbaknak előbb-utóbb szembe kell találkozniuk, hogy összemérjék az erejüket. De ahol King Kong és Godzilla csatázik, ott senki más nincs biztonságban.\r\n\r\nGodzilla nemrég legyőzte a többi titánt, és aki életben maradt, az mind engedelmes szolgája lett. De a Koponya-szigeten élő magányos óriás, Kong nem tartozik a hordába. És a 70-es években lezajlott, szerencsétlen végű expedíció óta csak egyre nagyobb és erősebb lett.', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/odM92ap8_c0\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(4, 'Monster Hunter – Szörnybirodalom', 8, '103', 'Diego Boneta Meagan Good Josh Helman Tony Jaa Milla Jovovich Ron Perlman T.I.', '<p>Artemis hadnagy (Milla Jovovich) &eacute;s emberei egy t&aacute;voli bolyg&oacute;ra ker&uuml;lnek &ndash; &eacute;s az &uacute;j vil&aacute;g r&eacute;gi lak&oacute;i nem fogadj&aacute;k őket szeretettel. A hatalmas, elk&eacute;pesztő erejű, megh&ouml;kkentő harci technik&aacute;kkal felszerelt sz&ouml;rnyek k&eacute;rd&eacute;s n&eacute;lk&uuml;l, azonnal v&eacute;gezni akarnak l&aacute;tsz&oacute;lag v&eacute;dtelen ellenfeleikkel.</p>\r\n<p>&Aacute;m az emberek kis csapata nem adja fel k&ouml;nnyen. &Eacute;letben akarnak maradni, b&aacute;rmi &aacute;ron. &Eacute;s min&eacute;l t&ouml;bben halnak meg k&ouml;z&uuml;l&uuml;k, ann&aacute;l elsz&aacute;ntabban harcolnak tov&aacute;bb a kiismerhetetlen terepen. Egyetlen c&eacute;ljuk maradt: a t&uacute;l&eacute;l&eacute;s.</p>', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/3od-kQMTZ9M\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(5, 'A Sólyom végveszélyben', 1, '144', 'Josh Hartnett, Ewan McGregor, Tom Sizemore', 'Amikor az amerikai hadsereg megpróbálja elfogni egy szomáli hadúr két emberét, a helikopterüket lelövik. Az amerikaiak súlyos veszteségeket szenvednek.', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/tnV6wM-vd9s\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(6, 'Bosszúállók: Végjáték', 3, '174', 'Karen Gillan, Robert Downey Jr., Bradley Cooper, Chris Evans, Brie Larson, Mark Ruffalo, Chris Hemsworth, Scarlett Johansson, Evangeline Lilly', 'A Bosszúállók-saga negyedik része a Marvel-filmek csúcspontja, egy epikus utazás vége. Thanos, a gonosz félisten elpusztította, porrá változtatta az univerzum felét, és megtörte a Bosszúállókat. A világ megmaradt szuperhősei megértik, mennyire törékeny a valóságuk, mennyire nem áll jól az általuk képviselt ügy. Thornak, a Fekete Özvegynek, Amerika Kapitánynak és Bruce Bannernek ki kell találnia a módját, hogyan hozzák vissza a legyőzött szövetségeseiket egy végső leszámolásra Thanosszal.', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/0iMRZ-P2yJA\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(7, 'Vissza hozzád', 6, '117', 'Michelle Monaghan,James Marsden,Luke Bracey', 'Amikor egy temetésen viszontlátják egymást, a középiskolás szerelem újra fellángol köztük, de az emlékek talán túl fájdalmasak ahhoz, hogy lehessen közös jövőjük.', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/--lzkRlUBSs\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(8, 'Venom', 8, '112', 'Tom Hardy, Michelle Williams, Riz Ahmed', 'Egy riporter élet-halál harcot vív egy őrült tudóssal, miután egy űrlény megszállja a testét, és különleges szupererővel ruházza fel.', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/SF49RxOgNE0\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(9, 'Annabelle', 4, '98', 'Annabelle Wallis, Ward Horton, Tony Amendola', 'John (Ward Horton) különleges ajándékot talál feleségének, Miának (Annabelle Wallis). Egy hófehér menyasszonyi ruhába öltöztetett, ritka, régi porcelánbabát. Ám Mia nem örülhet sokáig az ajándéknak, mert a babának szörnyű titka van. S egy borzalmas éjszakán a titokra fény derül. Egy sátánista, baljós szeánszon kiszabadul a babába zárt gonoszt, Annabelle.', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/OtG0rdBjRkU\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(10, 'Fűrész', 5, '90', 'Leigh Whannell, Cary Elwes, Danny Glover, Ken Leung, Dina Meyer, Michael Emerson, Shawnee Smith, Makenzie Vega, Monica Potter, Tobin Bell', 'Mit tennél meg azért, hogy mentsd az életed? Készülj egy idegborzoló utazásra a terror legsötétebb bugyraiba. A megháborodott szadista sorozatgyilkos áldozatait megszállottan az élet értékére akarja megtanítani úgy, hogy erkölcsileg szilárd embereket rabol el, és hátborzongató játékra kényszeríti őket, melynek tétje a saját életük. Az áldozatok mindegyikének képtelen döntési helyzetekkel szembesülve kell megküzdenie azért, hogy visszanyerje életét, vagy ellenkező esetben belehaljon a próbálkozásokba. ', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/yX0MQq4rrAk\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(11, 'Igen nap', 2, '89', 'Jennifer Garner,Edgar Ramírez,Jenna Ortega', 'A szülők, akik általában nemmel felelnek gyermekeik legvadabb kívánságaira, ma pár szabály megtartásával igennel válaszolnak, hajmeresztően őrült kalandokba keveredve.', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/H3r7kLAm514\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `intervals`
--

CREATE TABLE `intervals` (
  `ID` int(11) NOT NULL,
  `intervallum` varchar(100) COLLATE utf8mb4_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `intervals`
--

INSERT INTO `intervals` (`ID`, `intervallum`) VALUES
(1, '9:00 - 11:30'),
(2, '12:00 - 14:30'),
(3, '15:00 - 17:30'),
(4, '18:00 - 20:30');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `menu`
--

CREATE TABLE `menu` (
  `ID` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_hungarian_ci NOT NULL,
  `icon` varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  `param` varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  `parentID` int(11) NOT NULL,
  `queue` int(11) NOT NULL,
  `rights` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `menu`
--

INSERT INTO `menu` (`ID`, `name`, `icon`, `param`, `parentID`, `queue`, `rights`) VALUES
(2, 'Kategóriák kezelése', 'list-ul', 'categories_list', 0, 5, 1),
(3, 'Felhasználók kezelése', 'people-fill', 'users_list', 0, 40, 1),
(4, 'Profil módosítása', 'gear-wide', 'users_profilmod', 0, 60, 2),
(5, 'Filmek kezelése', 'film', 'films_list', 0, 10, 1),
(6, 'Korábbi foglalásaim', 'book-fill', 'reservations_list', 0, 50, 2),
(7, 'Jelszó módosítás', 'key-fill', 'users_passmod', 0, 70, 2),
(8, 'Jegytípusok kezelése', 'credit-card-2-front', 'tickets_list', 0, 20, 1),
(9, 'Vetítések kezelése', 'camera-reels-fill', 'projections_list', 0, 15, 1),
(10, 'Főoldal', 'house-door-fill', 'home', 0, 1, 2),
(11, 'Jegyfoglalás', 'cash-stack', 'reservation_new', 0, 45, 2),
(12, 'Vetítés naptár', 'calendar-fill', 'projections_calendar', 0, 43, 2);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `projections`
--

CREATE TABLE `projections` (
  `ID` int(11) NOT NULL,
  `filmID` int(11) NOT NULL,
  `date` date NOT NULL,
  `intervalID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `projections`
--

INSERT INTO `projections` (`ID`, `filmID`, `date`, `intervalID`) VALUES
(1, 6, '2021-03-20', 1),
(2, 6, '2021-03-20', 2),
(4, 6, '2021-03-20', 4);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `reservations`
--

CREATE TABLE `reservations` (
  `ID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `projectID` int(11) NOT NULL,
  `ticketID` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `ticketnumber` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- A tábla adatainak kiíratása `reservations`
--

INSERT INTO `reservations` (`ID`, `userID`, `projectID`, `ticketID`, `date`, `ticketnumber`) VALUES
(6, 2, 1, 1, '2021-03-17 16:29:28', 25),
(9, 2, 1, 1, '2021-03-17 16:36:15', 37),
(10, 2, 1, 1, '2021-03-17 16:36:15', 17),
(11, 2, 1, 1, '2021-03-17 16:36:16', 13),
(12, 2, 1, 1, '2021-03-17 16:36:17', 43),
(14, 2, 1, 1, '2021-03-17 17:18:24', 77),
(15, 2, 1, 1, '2021-03-17 17:18:24', 76),
(16, 2, 1, 1, '2021-03-17 17:18:25', 75),
(17, 2, 1, 1, '2021-03-17 17:48:57', 40),
(18, 1, 1, 1, '2021-03-17 17:52:26', 100),
(19, 2, 1, 1, '2021-03-17 17:52:30', 60),
(20, 2, 1, 1, '2021-03-17 17:52:32', 61),
(21, 2, 1, 1, '2021-03-17 17:55:06', 1),
(32, 1, 1, 1, '2021-03-17 17:57:56', 21),
(36, 2, 1, 1, '2021-03-17 18:03:00', 4),
(37, 2, 1, 1, '2021-03-17 18:04:00', 24),
(38, 2, 1, 1, '2021-03-17 18:04:02', 25),
(39, 2, 1, 1, '2021-03-17 18:04:03', 26),
(40, 2, 1, 1, '2021-03-17 18:04:03', 27),
(41, 2, 1, 1, '2021-03-17 18:04:06', 28),
(42, 2, 1, 1, '2021-03-17 18:04:37', 34),
(43, 2, 1, 1, '2021-03-17 18:04:37', 35),
(44, 2, 1, 1, '2021-03-17 18:04:38', 36),
(45, 2, 1, 1, '2021-03-17 18:04:39', 44),
(46, 2, 1, 1, '2021-03-17 18:04:40', 45),
(47, 2, 1, 1, '2021-03-17 18:04:41', 46),
(48, 1, 1, 1, '2021-03-17 18:04:45', 5),
(49, 1, 1, 1, '2021-03-17 18:04:45', 6),
(50, 1, 1, 1, '2021-03-17 18:04:46', 7),
(51, 1, 1, 1, '2021-03-17 18:04:46', 8),
(52, 2, 1, 1, '2021-03-17 18:11:53', 10),
(53, 2, 1, 1, '2021-03-17 18:11:56', 20),
(54, 2, 1, 1, '2021-03-17 18:11:57', 30),
(55, 1, 1, 1, '2021-03-17 18:12:09', 95),
(56, 1, 1, 1, '2021-03-17 18:12:10', 96),
(57, 1, 1, 1, '2021-03-17 18:12:10', 97),
(58, 1, 1, 1, '2021-03-17 18:12:11', 94),
(59, 2, 1, 1, '2021-03-17 18:13:24', 91),
(60, 2, 1, 1, '2021-03-17 18:17:58', 92),
(61, 2, 1, 1, '2021-03-17 18:18:07', 92),
(62, 2, 1, 1, '2021-03-17 18:18:35', 82),
(63, 2, 1, 1, '2021-03-17 18:18:38', 82),
(64, 2, 1, 1, '2021-03-17 18:19:33', 81),
(65, 2, 1, 1, '2021-03-17 18:19:35', 81),
(66, 2, 1, 1, '2021-03-17 18:22:43', 66),
(67, 2, 1, 1, '2021-03-17 18:22:51', 66),
(68, 2, 1, 1, '2021-03-17 18:23:19', 67),
(87, 2, 1, 1, '2021-03-17 18:27:19', 69),
(88, 2, 1, 1, '2021-03-17 18:27:19', 68),
(89, 2, 1, 1, '2021-03-17 18:27:21', 70),
(90, 2, 1, 1, '2021-03-17 18:27:34', 80),
(91, 2, 1, 1, '2021-03-17 18:27:35', 79),
(92, 2, 1, 1, '2021-03-17 18:27:36', 78),
(140, 2, 1, 1, '2021-03-18 09:03:27', 2),
(141, 2, 1, 1, '2021-03-18 09:03:28', 3),
(142, 2, 1, 1, '2021-03-18 09:03:30', 9),
(155, 2, 1, 1, '2021-03-18 09:09:12', 14),
(156, 2, 1, 1, '2021-03-18 09:09:13', 15),
(157, 2, 1, 1, '2021-03-18 09:09:13', 16),
(158, 1, 1, 1, '2021-03-18 09:09:15', 19),
(159, 1, 1, 1, '2021-03-18 09:09:16', 18),
(160, 1, 1, 1, '2021-03-18 09:09:17', 12),
(161, 1, 1, 1, '2021-03-18 09:09:17', 11);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `rights`
--

CREATE TABLE `rights` (
  `ID` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `rights`
--

INSERT INTO `rights` (`ID`, `name`) VALUES
(1, 'Adminisztrátor'),
(2, 'Felhasználó');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tickets`
--

CREATE TABLE `tickets` (
  `ID` int(11) NOT NULL,
  `category` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `tickets`
--

INSERT INTO `tickets` (`ID`, `category`, `price`) VALUES
(2, 'VIP', 1000),
(3, 'Normál', 1200),
(5, 'Diákjegy', 800);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(40) COLLATE utf8_hungarian_ci NOT NULL,
  `reg` datetime NOT NULL,
  `last` datetime DEFAULT NULL,
  `rights` int(11) NOT NULL,
  `status` set('1') COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`ID`, `name`, `email`, `password`, `reg`, `last`, `rights`, `status`) VALUES
(1, 'Teszt Elek', 'teszt@teszt.hu', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8', '2021-03-16 09:38:06', '2021-03-18 09:08:55', 2, '1'),
(2, 'Adminisztrátor', 'admin@admin.hu', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8', '2021-03-16 09:55:59', '2021-03-18 08:57:55', 1, '1'),
(3, 'Teszt 2', 'teszt2@onlinemozi.hu', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8', '2021-03-17 11:15:03', NULL, 2, '1');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `rights` (`rights`);

--
-- A tábla indexei `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `rights`
--
ALTER TABLE `rights`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ID`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `categories`
--
ALTER TABLE `categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT a táblához `films`
--
ALTER TABLE `films`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT a táblához `menu`
--
ALTER TABLE `menu`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT a táblához `reservations`
--
ALTER TABLE `reservations`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT a táblához `rights`
--
ALTER TABLE `rights`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`rights`) REFERENCES `rights` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
