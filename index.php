<?php
    ob_start();
    session_start();
    require("adatok.php");
    require("databaseClass.php");
    $db = new db($dbhost, $dbname, $dbuser, $dbpass);
?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- css styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" >
    <link rel="stylesheet" href="css/animations.css">
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="css/fullcalendar.main.min.css">
    <link rel="stylesheet" href="css/lightbox.css">
    <link rel="stylesheet" href="css/onlinemozi.css"> <!-- nincs létrehozva csak adtam neki egy nevet -->
    <link rel="stylesheet" href="css/mozijegy.css">
    <!-- css styles -->

    <!-- scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/canvasjs.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap4.min.js "></script>
    <script src="js/fullcalendar.main.min.js"></script>
    <script src="js/fullcalendar.locales-all.min.js"></script>
    <script src="js/tinymce.min.js" ></script>
    <script src="js/lightbox.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
	<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <script src="js/qrcode.js"></script>
    <script src="js/app.js"></script>
    <!-- scripts -->
</head>
<body>
    <header class="blog-header py-3 container-fluid">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-4 pt-1">

            </div>

            <div class="col-4 text-center">
                <a class="blog-header-logo" href="index.php" style="text-decoration: none"><?php echo $pagename; ?></a>
            </div>
            <div class="col-4 d-flex justify-content-end align-items-center">
                
            </div>
        </div>
    </header>
    
    <main role="main" class="container-fluid">
        <div class="row">
            <aside class="col-md-2 sidebar">
                <?php include 'admin/categories/categories_show.php'; ?>
            </aside>
            <div class="col-md-8 blog-main">
            <?php
                
            ?>
            <div id="showFilm">
                <?php include 'loader.php'; ?>
            </div>
            </div>
            <aside class="col-md-2">     
                <?php 
                    include 'users/users_login.php'; 
                    include 'search.php';
                ?>
            </aside>
        </div>
    </main>
    
    <footer class="containter-fluid blog-footer text-center">
        <?php   echo $company.' - '.$author; ?>
    </footer>
</body>
</html>