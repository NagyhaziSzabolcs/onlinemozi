<?php 
    // ha létezik a süti, akkor létrehozzuk a munkamenet változókat
    if (isset($_COOKIE['cid']) && !isset($_SESSION['cID']))
    {
        $_SESSION['cID'] = $_COOKIE['cid'];
        $_SESSION['cname'] = $_COOKIE['cname'];
        $_SESSION['cmail'] = $_COOKIE['cmail'];
        $_SESSION['cphone'] = $_COOKIE['cphone'];
        $_SESSION['cship'] = $_COOKIE['cship'];
        $_SESSION['cbill'] = $_COOKIE['cbill'];
        $_SESSION['ctax'] = $_COOKIE['ctax'];
        $_SESSION['creg'] = $_COOKIE['creg'];
        $_SESSION['clast'] = $_COOKIE['clast'];
       
    }

    if (!isset($_SESSION['cID']))
    {
        // ha rákattintottunk a belépés gombra
        if (isset($_POST['login_']))
        {
            $email =  $db->escapeString($_POST['email']);
            $pass =  $db->escapeString($_POST['pass']);
            if (empty($email) || empty($pass))
            {
                $db->showMessage('Nem adtál meg minden adatot!','danger');
            }
            else
            {
                $db->DBquery("SELECT * FROM customers WHERE email='$email'");
                if ($db->numRows() == 0)
                {
                    $db->showMessage('Nem regisztrált e-mail cím!','danger');
                }
                else
                {
                    $res = $db->fetchOne();
                    if ($res['password'] != SHA1($pass))
                    {
                        $db->showMessage('Hibás jelszó!','danger');
                    }
                    else
                    {
                        if ($res['status'] == 0)
                        {
                            $db->showMessage("Ez a felhasználó tiltott!", "danger");
                        }
                        else
                        {
                            $db->DBquery("UPDATE customers SET last=CURRENT_TIMESTAMP WHERE ID=".$res['ID']);
                       
                            $db->DBquery("SELECT * FROM customers WHERE email='$email'");
                            $res = $db->fetchOne();
    
                            $_SESSION['cID'] = $res['ID'];
                            $_SESSION['cname'] = $res['name'];
                            $_SESSION['cmail'] = $res['email'];
                            $_SESSION['cphone'] = $res['phone'];
                            $_SESSION['cship'] = $res['shipping_address'];
                            $_SESSION['cbill'] = $res['billing_address'];
                            $_SESSION['creg'] = $res['reg'];
                            $_SESSION['ctax'] = $res['taxnumber'];
                            $_SESSION['clast'] = $res['last'];
                           
                           
    
                            // ha bepipáltuk a bejelentkezve maradok-ot
                            if (isset($_POST['rememberme']))
                            {
                                // létrehozzuk a sütiket a munkamenet változók alapján (30 napra)
                                setcookie('cid', $_SESSION['cID'], time() + (86400 * 30), '/');
                                setcookie('cname', $_SESSION['cname'], time() + (86400 * 30), '/');
                                setcookie('cmail', $_SESSION['cmail'], time() + (86400 * 30), '/');
                                setcookie('cphone', $_SESSION['cphone'], time() + (86400 * 30), '/');
                                setcookie('cship', $_SESSION['cship'], time() + (86400 * 30), '/');
                                setcookie('cbill', $_SESSION['cbill'], time() + (86400 * 30), '/');
                                setcookie('ctax', $_SESSION['ctax'], time() + (86400 * 30), '/');
                                setcookie('creg', $_SESSION['creg'], time() + (86400 * 30), '/');
                                setcookie('clast', $_SESSION['clast'], time() + (86400 * 30), '/');
                                
                            }
                                                     
                            header('location:index.php');  
                        }
                    }
                }
            }
        }

        if (isset($_POST['_reg']))
        {
            header("location: index.php?pg=".base64_encode('customers_registration'));
        }


        // legeneráljuk a bejelentkezés űrlapot 
        $db->toForm('action|index¤
        name|Belépés¤
        email|email|E-mail cím¤
        password|pass|Jelszó¤
        checkbox|rememberme|Bejelentkezve maradok¤
        submit|login_|Belépek¤
        submit|_reg|Regisztráció');
    }
    else
    {
        if (isset($_POST['logout']))
        {
            // ha létezik sütink
            if (isset($_COOKIE['cid']))
            {
                // meszüntetjük a sütiket
                unset($_COOKIE['cid']); 
                unset($_COOKIE['cname']);
                unset($_COOKIE['cmail']);
                unset($_COOKIE['creg']);
                unset($_COOKIE['clast']);
                unset($_COOKIE['cphone']); 
                unset($_COOKIE['cship']); 
                unset($_COOKIE['cbill']); 
                unset($_COOKIE['ctax']); 
    
                setcookie('cid', '', time() - 3600, '/'); 
                setcookie('cname', '', time() - 3600, '/'); 
                setcookie('cmail', '', time() - 3600, '/');
                setcookie('creg', '', time() - 3600, '/');
                setcookie('clast', '', time() - 3600, '/');
                setcookie('cphone', '', time() - 3600, '/');
                setcookie('cship', '', time() - 3600, '/');
                setcookie('cbill', '', time() - 3600, '/');
                setcookie('ctax', '', time() - 3600, '/');
            }
            unset($_SESSION['cID']);
            unset($_SESSION['cname']);
            unset($_SESSION['cmail']);
            unset($_SESSION['creg']);
            unset($_SESSION['clast']);
            unset($_SESSION['cphone']);
            unset($_SESSION['cship']);
            unset($_SESSION['cbill']);
            unset($_SESSION['ctax']);

            header('location:index.php');
        }

            if (isset($_POST['cart3'])) 
            {
               header("location: index.php?pg=".base64_encode('customers_cart'));
            }

            $db->DBquery("SELECT count(*) AS 'db' FROM carts WHERE customerID=".$_SESSION['cID']);
            $res = $db->fetchOne();

        $db->toForm('name|Belépve¤
        action|index¤
        iconbutton|cart3|'.$res['db'].' tétel a kosárban¤
        text|uname|'.$_SESSION['cname'].'|disabled¤
        text|umail|'.$_SESSION['cmail'].'|disabled¤
        submit|logout|Kilépés');

        include("admin/customers/customers_menu.php");
        
    }
?>