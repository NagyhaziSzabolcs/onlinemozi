<?php
    if (isset($_POST['regisztracio']))
    {
        // inputok védelme
        $fullname =  $db->escapeString($_POST['name']);
        $email =  $db->escapeString($_POST['email']);
        $phone = $db->escapeString($_POST['phone']);
        $shipping_address = $db->escapeString($_POST['shipping_address']);
        $billing_address = $db->escapeString($_POST['billing_address']);
        $tax = $db->escapeString($_POST['tax']);
        $pass1 =  $db->escapeString($_POST['pass1']);
        $pass2 =  $db->escapeString($_POST['pass2']);

        if (empty($fullname) || empty($email) || empty($billing_address) || empty($pass1) || empty($pass2))
        {
            $db->showMessage('Nem adtál meg minden kötelező adatot!', 'danger');
        }
        else
        {
            if ($pass1 != $pass2)
            {
                $db->showMessage('A megadott jelszavak nem egyeznek!', 'danger');
            }
            else
            {
                $db->DBquery("SELECT * FROM customers WHERE email='$email'");
                if ($db->numRows() != 0)
                {
                    $db->showMessage('Ez az e-mail cím már foglalt!', 'danger');
                }
                else
                {
                    if (!isset($_POST['regfelt']))
                    {
                        $db->showMessage('Nem fogadtad el a regisztrációs szabályzatot!', 'danger');
                    }
                    else
                    {
                        $pass1 = SHA1($pass1);
                        if(empty($shipping_address))
                        {
                            $shipping_address = $billing_address;
                        }
                        $db->DBquery("INSERT INTO customers VALUES(null, '$fullname', '$email','$phone', '$pass1','$shipping_address','$billing_address','$tax', CURRENT_TIMESTAMP, null, '', 0,0,1)");
                        $db->showMessage('Sikeres regisztráció!', 'success');
                    }
                }
            }
        }
    }

    $db->toForm('name|Regisztráció¤
    action|customers_registration¤
    text|name|Teljes név¤
    email|email|E-mail cím¤
    text|phone|Telefonszám..¤
    text|billing_address|Számlázási cím¤
    text|shipping_address|Szállítási cím (ha nem egyezik a számlázási címmel)¤
    text|tax|Adószám¤
    password|pass1|Jelszó¤
    password|pass2|Jelszó megerősítése¤
    checkbox|regfelt|A regisztrációs szabályzatot elfogadom¤
    submit|regisztracio|Regisztráció');
?>  