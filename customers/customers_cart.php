<?php


        $db->logincheck('cID');

                //kosár ürítése
        if(isset($_POST['clear_']))
        {
                header("location: index.php?pg=".base64_encode('carts_empty'));
        }
                //rendelés leadása
        if(isset($_POST['_order']))
        {
                $paymentModes = $_POST['paymentModes'];
                $shippingModes = $_POST['shippingModes'];
                $description = $db->escapeString($_POST['description']);
                $customerID = $_SESSION['cID'];
                $summary = $_POST['summary'];

                if(empty($shippingModes) || empty($paymentModes))
                {
                        $db->showMessage('Nem adtál meg minden odatot!','danger');
                }
                else
                {
                        $db->DBquery("INSERT INTO orders VALUES(null, $customerID, CURRENT_TIMESTAMP, '$shippingModes', '$paymentModes', '$description', 0, '', null, $summary, '', 1)");
                        $orderID = $db->lastID();
                        $db->DBquery("SELECT * FROM carts WHERE customerID=".$customerID);
                        $items = $db->fetchAll();
                        foreach($items AS $item)
                        {
                                $productID = $item['productID'];
                                $amount = $item['amount'];
                                $res = $db->DBquery("SELECT 
                                categories.name AS 'catName',
                                brands.name AS 'brandName',
                                products.name AS 'productName',
                                products.price AS 'netto',
                                products.price * $afa * (1 + categories.spread / 100) AS 'brutto'
                                 FROM products 
                                 INNER JOIN brands ON brands.ID = products.brandID
                                 INNER JOIN categories ON categories.ID = products.catID
                                 WHERE products.ID=".$productID);

                                 $res = $db->fetchOne();

                                 $category = $res['catName'];
                                 $brand = $res['brandName'];
                                 $productname = $res['productName'];
                                 $price = $res['brutto'];
                                 $profit = (($price/$afa) - $res['netto']) * $amount;
                                $db->DBquery("INSERT INTO orderitems VALUES(
                                null, 
                                $orderID, 
                                $productID, 
                                '$category', 
                                '$brand',  
                                '$productname',
                                $amount,
                                $price,
                                $profit
                                )");
                        }

                        $db->DBquery("DELETE FROM carts WHERE customerID=".$customerID);
                        header('location: index.php?pg='.base64_encode('customers_cart'));
                }
        }


        $db->DBquery("SELECT
        `Kosárban lévő termékek`.ID AS '.ID',
        categories.name AS Kategória,
        brands.name AS 'Gyártó',
        products.name AS 'Terméknév',
        `Kosárban lévő termékek`.amount AS 'Menny.(db)',
        ((1 + (categories.spread/100)) * products.price * $afa) AS 'Egys.Ár($penznem)',
        ((1 + (categories.spread/100)) * products.price * $afa * `Kosárban lévő termékek`.amount) AS 'Össze.($penznem)'
        FROM carts AS `Kosárban lévő termékek`
        INNER JOIN products ON products.ID = `Kosárban lévő termékek`.productID
        INNER JOIN brands ON brands.ID = products.brandID
        INNER JOIN categories ON categories.ID = products.catID
        WHERE `Kosárban lévő termékek`.customerID=".$_SESSION['cID']);

        $db->toTable('u|d');


        if($db->numRows() != 0)
        {
                $db->DBquery("SELECT 
                SUM( ((1 + (categories.spread/100)) * products.price * $afa * carts.amount)) AS summary
                FROM carts
                INNER JOIN products ON products.ID = carts.productID
                INNER JOIN categories ON categories.ID = products.catID
                WHERE carts.customerID=".$_SESSION['cID']);
        
                $res = $db->fetchOne();

                echo '<br>
                A kosrában lévő termékek összértéke: <strong>'.$db->numberFormat($res['summary']).' '.$penznem.'</strong>
                <br><br>';

        $db->toForm('name|Rendelés¤
        action|customers_cart¤
        label|x|Fizetés módja: <em>*</em>¤
        select|paymentModes|name|name¤
        label|x|Szállítás módja:<em>*</em>¤
        select|shippingModes|name|name¤
        label|x|Megjegyzés:¤
        textarea|description¤
        hidden|summary|'.$res['summary'].'¤
        label|x|A *-al jelölt mezők megadása kötelező!¤
        submit|clear_|Kosár ürítése¤
        submit|_order|Rendelés leadása');

        }


?>