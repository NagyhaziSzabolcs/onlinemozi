<?php
    //$db->logincheck('uID');

    if(isset($_POST['yes']))
    {
        //törlendő kép adatai
        $db->DBquery("SELECT * FROM attachments WHERE ID=".$id);
        $res = $db->fetchOne();
        //termék azonosító
        $filmID = $res['filmID'];

        //ha létezik töröljük
        if(is_file($res['dir'].'/'.$res['filename']))
        {
            unlink($res['dir'].'/'.$res['filename']);
        }
        //töröljük a táblából
        $db->DBquery("DELETE FROM attachments WHERE ID=".$id);
        //alapértelmezett kép törlése esetén a következő lesz alapértelmezett
        if($res['def'] == 1)
        {
            $db->DBquery("UPDATE attachments SET def = 1 WHERE ID=(SELECT ID FROM attachments WHERE filmID =".$filmID." LIMIT 0, 1)");
        }
        //vissza irányítjuk a termék listához
        header("location: index.php?pg=".base64_encode('Films_attach&id='.$filmID));
    }

    $db->toForm('name|Plakát törlése¤
    action|attachments_delete&id='.$id.'¤
    label|info|Biztosan törlöd?¤
    submit|yes|Törlés');

    include("attachments_info.php");
?>