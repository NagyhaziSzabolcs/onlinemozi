<?php

    //include "search.php";
    
    echo '<div id="showFilm">';
    echo '<h1>Film ajánlónk</h1>';

    $db->DBquery("SELECT DISTINCT
    films.ID AS '.aktID',
    films.name AS 'Cím',
    categories.name AS 'Kategória',
    films.actors AS 'Szereplők',
    films.length AS 'Hossz',
    (SELECT CONCAT(dir, '/', filename) FROM attachments WHERE filmID=`.aktID` AND def=1) AS 'pic'
    FROM films
    INNER JOIN categories ON categories.ID = films.catID 
    ORDER BY Rand() LIMIT 0, 12");

    $db->toGrid('Cím', 'Szereplők', 'Kategória', 'Hossz', 'pic', 'Films_show');
    echo '</div>';
?>
