<?php
    require 'TCPDF-main/tcpdf.php';
    $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->SetFont('freeserif', '', 11);
    $pdf->AddPage();

    echo '<h3>Jegyfoglalás</h3><hr>
    <button class="btn btn-primary" id="PDF_btn" onclick="getPDF()">Jegyek letöltése PDF-ben</button>
   <span id="genmsg" style="display:none;">PDF fájl generálása...</span>';

    $uid = $_SESSION['uID'];
    $projectionID = 1;

    $db->DBquery("SELECT  
      reservations.ID	AS 'ID',
      reservations.userID	AS 'userID',
      projections.date AS 'projection_date',
      intervals.intervallum AS 'projection_time',
      films.name AS 'title',
      tickets.category AS 'category',
      tickets.price AS 'price',	
      reservations.date	AS 'book_date',
      reservations.ticketnumber AS 'ticketnumber' 	
    FROM reservations 

    LEFT JOIN tickets ON tickets.ID = reservations.ticketID
    LEFT JOIN projections ON projections.ID = reservations.projectID


    LEFT JOIN films ON films.ID = projections.filmID
    LEFT JOIN intervals ON intervals.ID = projections.intervalID

    WHERE reservations.userID=$uid AND reservations.projectID=$projectionID");

    
    echo  '<div id="jegyek">';

    foreach($db->queryresult as $record)
    {
      echo '<div class="jegy">
        <div class="jegyadatok"><h2>Mozijegy</h2>
            <hr>
            <h5>Film címe: '.$record['title'].'</h5>
            <hr>
            <h5>Vetítés időpontja: '.$record['projection_date'].' '.$record['projection_time'].'</h5>
            <hr>
            <h5>Jegyadatok: '.$record['category'].' '.$record['price'].' '.$GLOBALS['penznem'].' | Sorszám: '.$record['ticketnumber'].'.</h5>
            <hr>
            <h5>Vásárlási adatok: '.$record['book_date'].'</h5>
        </div>
        <div class="qrcode" id="qrcodeID'.$record['ticketnumber'].'"></div>
      </div><hr>';
      $db->toQRCode($record['ticketnumber'], 'qrcodeID'.$record['ticketnumber'], '270|270|#000000|#ffffff|H');
    }
   echo '</div>';
?>