$(document).ready(function(){
    $('.alert-info').delay(3000).fadeOut(1500);
    $('.alert-warning').delay(3000).fadeOut(1500);
    $('.alert-danger').delay(3000).fadeOut(1500);
    $('.alert-success').delay(3000).fadeOut(1500);


    $('#myTable').DataTable({
        "pageLength": 25,
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false
        } ],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Hungarian.json"
        }        
    } );
  
    tinymce.init({
        selector:'.editor', 
        language: 'hu_HU'
    });

    $("#liveSearch").on("keyup",function()
    {
        var value = $(this).val();
        $(".card").each(function(result)
        {
            if(result != -1)
            {
                var id = $(this).find(".card-body:first").text();
                if(id.indexOf(value) != 0 && id.toLowerCase().indexOf(value.toLowerCase())<0)
                {
                    $(this).hide();
                }
                else
                {
                    $(this).show();
                }
                console.log(value);
            }
        });
        console.log(value);
    });



    $('.seat').on('mousemove', function(e){
        let selector = '#' + e.target.id;
        $(selector).css('font-weight', 'bold');
        $(selector).css('border-color', 'orange');
    });

    $('.seat').on('mouseleave', function(e){
        let selector = '#' + e.target.id;
        $(selector).css('font-weight', 'normal');
        $(selector).css('border-color', 'black');
    });

    $('.seat').on('click', function(e){
        let selector = '#' + e.target.id;
        if ($(selector).hasClass('reserved'))
        {
            alert('Ez a hely már foglalt!');
        }
        else
        {
            $.post("reservations/reservation.php", {param: e.target.id}, function(result){
                console.log(result);
                $(selector).toggleClass('selected');
            });
        }    
    });

    function getSeatsStatus()
    {
        $.post("reservations/seatsStatus.php", {}, function(result){
            let reservedseats = JSON.parse(result);

            for(i=0; i< 100; i++)
            {
                let selector = '#' + (i+1);
                if ($(selector).hasClass('reserved'))
                {
                    $(selector).removeClass('reserved');
                }
            }

            for(i=0; i< reservedseats.length; i++)
            {
                selector = '#' + parseInt(reservedseats[i]);
                if (!$(selector).hasClass('selected'))
                {
                    $(selector).addClass('reserved');
                }
            }
        })
    }

    setInterval(function(){getSeatsStatus()}, 500);




});

function filterFilms(ID){
    $.post("admin/films/films_filter.php",{param: ID},function(data){
        $('#showFilm').html(data);
    });
   $('#searchDiv').show();
}

function loadAll()
{
    $('#showFilm').load("admin/films/showAll.php");
}

// innen PFD


function getPDF(){
    var filename = 'jegyek';

    $("#PDF_btn").hide();
    $("#genmsg").show();
    var HTML_Width = $("#jegyek").width();
    var HTML_Height = $("#jegyek").height();
    var top_left_margin = 30;
    var PDF_Width = HTML_Width+(top_left_margin*2);
    var PDF_Height = (PDF_Width*1.414)+(top_left_margin*2);
    console.log(PDF_Width, PDF_Height);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;
    
    var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
    

    html2canvas($("#jegyek")[0],{allowTaint:true}).then(function(canvas) {
        canvas.getContext('2d');
    //	console.log('HTML width: '+HTML_Width+"  HTML height: "+HTML_Height);
    //	console.log('PDF width: '+PDF_Width+" PDF height "+PDF_Height);
    //	console.log('PDF total page: '+totalPDFPages);
    //	window.alert('x');
        var imgData = canvas.toDataURL("image/jpeg", 1.0);
        var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
        
        
        for (var i = 1; i <= totalPDFPages; i++) { 
            pdf.addPage(PDF_Width, PDF_Height);
            pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
        }
        
        pdf.save(filename+".pdf");
        
        setTimeout(function(){ 			
            $("#PDF_btn").show();
            $("#genmsg").hide();
        }, 0);

    });
};







// idáig PDF