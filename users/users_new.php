<?php
    $db->logincheck('uID');
    
    if (isset($_POST['insert']))
    {
        // inputok védelme
        $teljesnev = escapeshellcmd($_POST['nev']);
        $email = escapeshellcmd($_POST['email']);
        $pass1 = escapeshellcmd($_POST['pass1']);
        $pass2 = escapeshellcmd($_POST['pass2']);

        if (empty($teljesnev) || empty($email) || empty($pass1) || empty($pass2))
        {
            $db->showMessage('Nem adtál meg minden adatot!', 'danger');
        }
        else
        {
            if ($pass1 != $pass2)
            {
                $db->showMessage('A megadott jelszavak nem egyeznek!', 'danger');
            }
            else
            {
                $db->DBquery("SELECT * FROM users WHERE email='$email'");
                if ($db->numRows() != 0)
                {
                    $db->showMessage('Ez az e-mail cím már foglalt!', 'danger');
                }
                else
                {
                    $pass1 = SHA1($pass1);
                    $db->DBquery("INSERT INTO users VALUES(null, '$teljesnev', '$email', '$pass1', CURRENT_TIMESTAMP, null, 2, 1)");
                    header("location: index.php?pg=".base64_encode('users_list'));
                }
            }
        }
    }

    if (isset($_POST['back']))
    {
        header("location: index.php?pg=".base64_encode('users_list'));
    }

    $db->toForm('name|Új felhasználó felvétele¤
    action|users_new¤
    text|nev|Teljes név¤
    email|email|E-mail cím¤
    password|pass1|Jelszó¤
    password|pass2|Jelszó megerősítése¤
    submit|insert|Felvesz¤
    submit|back|Vissza a listához');
?>  