<?php 

    if(isset($_SESSION['uID']))
    {
        if(isset($_POST['logout']))
        {
            header("location:index.php?pg=".base64_encode('users_logout'));
        }
        $db->DBquery("SELECT * FROM users WHERE ID =".$_SESSION['uID']);
        $res = $db->fetchOne();
        $db->toForm('name|Belépve¤
        action|index¤
        text|x|'.$res['name'].'|disabled¤
        text|x|'.$res['email'].'|disabled¤
        submit|logout|Kijelentkezés');
        include 'menu.php';
    }
    else
    {
        // ha rákattintottunk a belépés gombra
    if (isset($_POST['login_']))
    {
        $email = escapeshellcmd($_POST['email']);
        $pass = escapeshellcmd($_POST['pass']);
        if (empty($email) || empty($pass))
        {
            $db->showMessage('Nem adtál meg minden adatot!','danger');
        }
        else
        {
            $db->DBquery("SELECT * FROM users WHERE email='$email'");
            if ($db->numRows() == 0)
            {
                $db->showMessage('Nem regisztrált e-mail cím!','danger');
            }
            else
            {
                $res = $db->fetchOne();
                if ($res['password'] != SHA1($pass))
                {
                    $db->showMessage('Hibás jelszó!','danger');
                }
                else
                {
                    if ($res['status'] == null)
                    {
                        $db->showMessage("Ez a felhasználó tiltott!", "danger");
                    }
                    else
                    {
                        $db->DBquery("UPDATE users SET last=CURRENT_TIMESTAMP WHERE ID=".$res['ID']);
                    
                        $db->DBquery("SELECT * FROM users WHERE email='$email'");
                        $res = $db->fetchOne();

                        $_SESSION['uID'] = $res['ID'];
                        $_SESSION['uname'] = $res['name'];
                        $_SESSION['umail'] = $res['email'];
                        $_SESSION['ureg'] = $res['reg'];
                        $_SESSION['ulast'] = $res['last'];
                        $_SESSION['urights'] = $res['rights'];

                        // ha bepipáltuk a bejelentkezve maradok-ot
                        if (isset($_POST['rememberme']))
                        {
                            // létrehozzuk a sütiket a munkamenet változók alapján (30 napra)
                            setcookie('uid', $_SESSION['uID'], time() + (86400 * 30), '/');
                            setcookie('uname', $_SESSION['uname'], time() + (86400 * 30), '/');
                            setcookie('umail', $_SESSION['umail'], time() + (86400 * 30), '/');
                            setcookie('ureg', $_SESSION['ureg'], time() + (86400 * 30), '/');
                            setcookie('ulast', $_SESSION['ulast'], time() + (86400 * 30), '/');
                            setcookie('urights', $_SESSION['urights'], time() + (86400 * 30), '/');
                        }
                                                    
                        header('location:index.php');  
                    }
                }
            }
        }
    }

    // ha rákattintottunk a Regisztráció gombra
    if (isset($_POST['_reg']))
    {
        header("location:index.php?pg=".base64_encode('users_regisztracio'));
    }

    // legeneráljuk a bejelentkezés űrlapot 
    $db->toForm('action|index¤
    name|Belépés¤
    email|email|E-mail cím¤
    password|pass|Jelszó¤
    checkbox|rememberme|Bejelentkezve maradok¤
    submit|login_|Belépek¤
    submit|_reg|Regisztráció');
    }
    

?>